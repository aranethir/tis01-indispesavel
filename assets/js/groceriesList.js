var listsCollection = "";
if (localStorage.getItem('listsCollection')) {
    listsCollection = JSON.parse(localStorage.getItem('listsCollection'));
} else {
    listsCollection = {
        "lists": []
    };
}
localStorage.setItem('listsCollection', JSON.stringify(listsCollection));
const data = JSON.parse(localStorage.getItem('listsCollection'));
var listData = {
    "id": null,
    "date": null,
    "products": []
};
var updatedRow = '';

function addRow() {
    var table = document.getElementById("dynList");
    var row = table.insertRow(-1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);

    cell1.innerHTML = document.getElementById("prod").value;
    cell2.innerHTML = document.getElementById("qtty").value;
    cell3.innerHTML = document.getElementById("unitPick").value;
    cell4.innerHTML = '<button class="btn btn-danger btn-xs btn-simple" onclick="excludeInfo(this)">X</button>';
    addItem();
    document.getElementById("dataEntry").reset();
}
function excludeInfo(row) {
    var ind = row.parentNode.parentNode.rowIndex;
    document.getElementById("dynList").deleteRow(ind);
    listData.products.splice(ind - 1, 1);
}
function addItem() {
    var table = document.getElementById("dynList");
    for (var i = 1; i < table.rows.length; i++) {
        var listItem = {
            "name": table.rows[i].cells[0].innerHTML,
            "quantity": table.rows[i].cells[1].innerHTML,
            "unit": table.rows[i].cells[2].innerHTML
        }
    }

    listData.products.push(listItem);
    console.log(listData);

}
function excludeList(row) {
    //Removes the table row that displays the object.
    let ind = parseInt(row.parentNode.parentNode.rowIndex);
    document.getElementById("savedLists").deleteRow(ind - 1);
    //Removes the desired item from the object.
    listsCollection.lists = listsCollection.lists.filter(function (element) { return element.id != listsCollection.lists[ind].id });
    //Updates de Local Storage.
    localStorage.setItem('listsCollection', JSON.stringify(listsCollection));
    window.location.reload();
}
//Fazer função de exportar tudo para um outro object array que vai salvar todas as listas criadas.
function saveList(listData) {
    listData.id = prompt("Dê um nome para esta lista:");
    listData.date = dateGenerator();
    listsCollection.lists.push(listData);
    localStorage.setItem('listsCollection', JSON.stringify(listsCollection));
    location.href = "index.html";
}

//Gerar datas.
function dateGenerator() {
    var currentDate = new Date();
    return (`${currentDate.getDate()}/${currentDate.getMonth()+1}/${currentDate.getFullYear()} ${currentDate.getHours()}:${currentDate.getMinutes()}`);
}

// Get the modal
var modal = document.getElementById('mainModalArea');

// Get the button that opens the modal
var btn = document.getElementById("newList");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
document.getElementById("emptyList").onclick = function () {
    location.href = "groceries.html";
};
document.getElementById("listModels").onclick = function () {
    location.href = "templateLists.html";
};

function loadData() {
    document.getElementById("savedLists").innerHTML = '';
    for (let i = 0; i < listsCollection.lists.length; i++) {
        $("#savedLists").append(`<tr><td scope="row"><button class="btn btn-xs btn-success" onclick="seeDataDetailsModal(this)">Abrir</button></td></td>
        <td>${listsCollection.lists[i].date}</td>
        <td>${listsCollection.lists[i].id}</td>
        <td><button class="btn btn-xs btn-danger" onclick="excludeList(this)">Apagar</button></td>`);
    }
}

function setRow(row) {
    updatedRow = parseInt(row.parentNode.parentNode.rowIndex) - 1;
}

function seeDataDetailsModal(row) {
    var ind = parseInt(row.parentNode.parentNode.rowIndex);
    /*To fill the form use localData.localItems[ind] to pull the proper data from the contact that must be altered.
    Make the button call a modal with the data selected.*/
    modal.style.display = "block";
    document.getElementById("mainModalContent").innerHTML = `
                <p class="broadContent">Indispen$ável, sempre com você!</p>
            <div>
                <table class="groceryList">
                    <tbody id="viewList">
                        <tr>
                            <td></td>
                            <td><h4>Produto</h4></td>
                            <td><h4>Quantidade</h4></td>
                            <td><h4>Unidade</h4></td>
                        </tr>
                        <tr>
                        `
    for (let i = 0; i < listsCollection.lists[ind].products.length; i++) {
        $("#viewList").append(`<tr><td scope="row"><input type="checkbox"></td>
                            <td>${listsCollection.lists[ind].products[i].name}</td>
                            <td>${listsCollection.lists[ind].products[i].quantity}</td>
                            <td>${listsCollection.lists[ind].products[i].unit}</td>`)
    };
    `}
                        </tr>
                    </tbody>
                </table>
            </div>
    `;
}

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


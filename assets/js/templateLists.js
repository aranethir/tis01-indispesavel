const baseLists = {
    "templates": [{
        "value": "0",
        "templateData": [{
            "name": "Arroz Branco tipo 01",
            "quantity": 10,
            "unit": "Kg"
        },
        {
            "name": "Feijão Carioca",
            "quantity": 4,
            "unit": "Kg"
        },
        {
            "name": "Óleo de Soja",
            "quantity": 4,
            "unit": "Litro(s)"
        },
        {
            "name": "Macarrão Spaghetti",
            "quantity": 4,
            "unit": "Pacote(s)"
        },
        {
            "name": "Farinha de Mandioca",
            "quantity": 1,
            "unit": "Kg"
        },
        {
            "name": "Milho de Pipoca",
            "quantity": 1,
            "unit": "Kg"
        },
        {
            "name": "Sal",
            "quantity": 1,
            "unit": "Kg"
        },
        {
            "name": "Biscoito Água e Sal",
            "quantity": 2,
            "unit": "Pacote(s)"
        },
        {
            "name": "Biscoito Recheado",
            "quantity": 2,
            "unit": "Pacote(s)"
        },
        {
            "name": "Achocolatado",
            "quantity": 500,
            "unit": "Gr"
        },
        {
            "name": "Leite em Pó",
            "quantity": 500,
            "unit": "Gr"
        },
        {
            "name": "Goiabada Cascão",
            "quantity": 250,
            "unit": "Gr"
        }
        ]
    },
    {
        "value": "1",
        "templateData": [{
            "name": "Banana Caturra",
            "quantity": 1,
            "unit": "Cacho(s)"
        },
        {
            "name": "Maça Gala",
            "quantity": 5,
            "unit": "Unidade(s)"
        },
        {
            "name": "Laranja Serra D'água",
            "quantity": 4,
            "unit": "Unidade(s)"
        },
        {
            "name": "Melância",
            "quantity": 1,
            "unit": "Unidade(s)"
        },
        {
            "name": "Mamão",
            "quantity": 1,
            "unit": "Unidade(s)"
        }
        ]
    },
    {
        "value": "2",
        "templateData": [{
            "name": "Fraldas Descartáveis 12un",
            "quantity": 1,
            "unit": "Pacote(s)"
        },
        {
            "name": "Lenços Umidecidos",
            "quantity": 1,
            "unit": "Pacote(s)"
        },
        {
            "name": "Algodão",
            "quantity": 1,
            "unit": "Pacote(s)"
        },
        {
            "name": "Cotonete",
            "quantity": 1,
            "unit": "Caixa(s)"
        },
        {
            "name": "Xampu",
            "quantity": 1,
            "unit": "Embalagem(s)"
        },
        {
            "name": "Sabonete",
            "quantity": 1,
            "unit": "Embalagem(s)"
        },
        {
            "name": "Colônia",
            "quantity": 1,
            "unit": "Embalagem(s)"
        }
        ]
    },
    {
        "value": "3",
        "templateData": [{
            "name": "Desinfetante",
            "quantity": 1,
            "unit": "Embalagem(s)"
        },
        {
            "name": "Álcool",
            "quantity": 1,
            "unit": "Litro(s)"
        },
        {
            "name": "Cloro",
            "quantity": 1,
            "unit": "Litro(s)"
        },
        {
            "name": "Água Sanitária",
            "quantity": 1,
            "unit": "Litro(s)"
        },
        {
            "name": "Detergente",
            "quantity": 1,
            "unit": "Unidade(s)"
        },
        {
            "name": "Sabão em Pó",
            "quantity": 1,
            "unit": "Caixa(s)"
        },
        {
            "name": "Amaciante",
            "quantity": 1,
            "unit": "Litro(s)"
        },
        {
            "name": "Sabão em Barras",
            "quantity": 1,
            "unit": "Pacote(s)"
        },
        {
            "name": "Perfumador de Ambientes",
            "quantity": 1,
            "unit": "Litro(s)"
        }
        ]
    },
    {
        "value": "4",
        "templateData": [{
            "name": "Desodorante",
            "quantity": 3,
            "unit": "Unidade(s)"
        },
        {
            "name": "Xampu",
            "quantity": 1,
            "unit": "Unidade(s)"
        },
        {
            "name": "Condicionador",
            "quantity": 1,
            "unit": "Unidade(s)"
        },
        {
            "name": "Lâmina de Barbear",
            "quantity": 2,
            "unit": "Pacote(s)"
        },
        {
            "name": "Loção Pós Barba",
            "quantity": 1,
            "unit": "Unidade(s)"
        }
        ]
    },
    {
        "value": "5",
        "templateData": [{
            "name": "Xampu",
            "quantity": 1,
            "unit": "Unidade(s)"
        },
        {
            "name": "Condicionador",
            "quantity": 1,
            "unit": "Unidade(s)"
        },
        {
            "name": "Creme de Pentear",
            "quantity": 1,
            "unit": "Unidade(s)"
        },
        {
            "name": "Creme Hidratante",
            "quantity": 1,
            "unit": "Unidade(s)"
        },
        {
            "name": "Sabonete Facial",
            "quantity": 3,
            "unit": "Unidade(s)"
        },
        {
            "name": "Protetor Solar",
            "quantity": 1,
            "unit": "Unidade(s)"
        },
        {
            "name": "Absorvente Noturno",
            "quantity": 1,
            "unit": "Pacote(s)"
        },
        {
            "name": "Absorvente Diurno",
            "quantity": 1,
            "unit": "Pacote(s)"
        },
        {
            "name": "Demaquilante",
            "quantity": 1,
            "unit": "Embalagem(s)"
        },
        {
            "name": "Discos de Algodão",
            "quantity": 1,
            "unit": "Embalagem(s)"
        }
        ]
    },
    {
        "value": "6",
        "templateData": [{
            "name": "Papel Higiênico",
            "quantity": 1,
            "unit": "Pacote(s)"
        },
        {
            "name": "Sabonete",
            "quantity": 5,
            "unit": "Unidade(s)"
        },
        {
            "name": "Creme Dental",
            "quantity": 4,
            "unit": "Unidade(s)"
        },
        {
            "name": "Cotonete",
            "quantity": 2,
            "unit": "Caixa(s)"
        },
        {
            "name": "Fio Dental",
            "quantity": 2,
            "unit": "Unidades(s)"
        },
        {
            "name": "Enxaguante Bucal",
            "quantity": 1,
            "unit": "Litro(s)"
        },
        {
            "name": "Bucha para Banho",
            "quantity": 1,
            "unit": "Unidade(s)"
        }
        ]
    },
    {
        "value": "7",
        "templateData": [{
            "name": "Picanha Bovina",
            "quantity": 2,
            "unit": "Kg"
        },
        {
            "name": "Alcatra Bovina",
            "quantity": 3,
            "unit": "Kg"
        },
        {
            "name": "Asa de Frango",
            "quantity": 2,
            "unit": "Kg"
        },
        {
            "name": "Linguína Suína",
            "quantity": 2,
            "unit": "Kg"
        },
        {
            "name": "Pernil Suíno s/ Osso",
            "quantity": 2,
            "unit": "Kg"
        },
        {
            "name": "Cerveja",
            "quantity": 12,
            "unit": "Lata(s)"
        },
        {
            "name": "Refrigerante Cola",
            "quantity": 6,
            "unit": "Litro(s)"
        },
        {
            "name": "Sal Grosso",
            "quantity": 1,
            "unit": "Pacote(s)"
        },
        {
            "name": "Carvão Cegetal",
            "quantity": 1,
            "unit": "Pacote(s)"
        },
        {
            "name": "Pão de Alho",
            "quantity": 6,
            "unit": "Unidade(s)"
        },
        {
            "name": "Arroz Branco",
            "quantity": 5,
            "unit": "Kg"
        },
        {
            "name": "Tomate",
            "quantity": 5,
            "unit": "Unidade(s)"
        },
        {
            "name": "Pimentão Verde",
            "quantity": 3,
            "unit": "Unidade(s)"
        },
        {
            "name": "Cebola Amarela",
            "quantity": 2,
            "unit": "Unidade(s)"
        },
        {
            "name": "Vinagre",
            "quantity": 1,
            "unit": "Litro(s)"
        }
        ]
    }
    ]
};
//Insere os dados na tabela de acordo com o modelo que o usuário selecionou.
function applyList() {

    for (let index = 0; index < baseLists.templates[document.getElementById("tempSelect").value].templateData.length; index++) {
        var table = document.getElementById("dynList");
        var row = table.insertRow(-1);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        
        cell1.innerHTML = baseLists.templates[document.getElementById("tempSelect").value].templateData[index].name;
        cell2.innerHTML = baseLists.templates[document.getElementById("tempSelect").value].templateData[index].quantity;
        cell3.innerHTML = baseLists.templates[document.getElementById("tempSelect").value].templateData[index].unit;
        cell4.innerHTML = '<button class="btn btn-danger btn-xs btn-simple" onclick="excludeInfo(this)">X</button>';

        addItem();
    }
    
}
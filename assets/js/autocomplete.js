function autocomplete(inp, arr) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
          /*check if the item starts with the same letters as the text field value:*/
          if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
            /*create a DIV element for each matching element:*/
            b = document.createElement("DIV");
            /*make the matching letters bold:*/
            b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
            b.innerHTML += arr[i].substr(val.length);
            /*insert a input field that will hold the current array item's value:*/
            b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
            /*execute a function when someone clicks on the item value (DIV element):*/
            b.addEventListener("click", function(e) {
                /*insert the value for the autocomplete text field:*/
                inp.value = this.getElementsByTagName("input")[0].value;
                /*close the list of autocompleted values,
                (or any other open lists of autocompleted values:*/
                closeAllLists();
            });
            a.appendChild(b);
          }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
          /*If the arrow DOWN key is pressed,
          increase the currentFocus variable:*/
          currentFocus++;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 38) { //up
          /*If the arrow UP key is pressed,
          decrease the currentFocus variable:*/
          currentFocus--;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 13) {
          /*If the ENTER key is pressed, prevent the form from being submitted,*/
          e.preventDefault();
          if (currentFocus > -1) {
            /*and simulate a click on the "active" item:*/
            if (x) x[currentFocus].click();
          }
        }
    });
    function addActive(x) {
      /*a function to classify an item as "active":*/
      if (!x) return false;
      /*start by removing the "active" class on all items:*/
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      /*add class "autocomplete-active":*/
      x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
      /*a function to remove the "active" class from all autocomplete items:*/
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
      }
    }
    function closeAllLists(elmnt) {
      /*close all autocomplete lists in the document,
      except the one passed as an argument:*/
      var x = document.getElementsByClassName("autocomplete-items");
      for (var i = 0; i < x.length; i++) {
        if (elmnt != x[i] && elmnt != inp) {
          x[i].parentNode.removeChild(x[i]);
        }
      }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
  }


/*An array containing all the products we need:*/
var list = [ "Água Sanitária","Cloro","Álcool","Amaciante de Roupas","Cêra","Desengordurante",
"Desinfetante","Detergente","Esponja","Inseticida","Palha de Aço","Limpa Alumínio","Limpa Forno",
"LImpa Vidros","Lustra Móveis","Multiuso","Pedra Sanitária","Sabão em Barra","Sabão de Côco",
"Sabão em Pó","Sabão Líquido","Saco de lixo","Sapólio","Filtro para Café","Fósforo","Palito de Dente",
"Vela","Guardanapo","Papel Toalha","Papel Alumínio","Papel Filme","Papel Manteiga","Vassoura",
"Rodo","Pano de Limpeza","Alho","Azeite","Caldo de Galinha","Caldo de Carne","Canela","Cravo","Louro",
"Creme de Cebola","Ketchup","Mostarda","Molho de Pimenta","Molho Inglês","Shoyu","Orégano","Molho Pronto",
"Picles","Pimenta do Reino","Sal Refinado","Sal Grosso","Vinagre","Absorvente","Acetona","Esmalte",
"Água Oxigenada","Amônia","Algodão","Anti-séptico Bucal","Cera Depiladora","Condicionador","Cotonete",
"Creme de Barbear","Creme de Pentear","Creme de Tratamento","Creme Dental","Creme Hidratante",
"Desodorante","Escona Dental","Fio Dental","Fralda","Gel para Cabelo","Pós-Barba","Lâmina de Barbear",
"Lenço Umedecido","Papel Higiênico","Protetor Solar","Recipiente","Sabonete","Sabonete Líquido",
"Shampoo","Tintura de Cabelo","Carne Bovina Moída","Bife de Acém","Bife de Patinho","Bife de Contra Filé",
"Costela Suína","Lombo Suíno","Costela Bovina","Coxa e Sobrecoxa de Frango","Hambúrguer",
"Linguiça","Salsicha","Bacon","Músculo","Fígado Bovino","Peito de Frango","Moelinha","Peixe",
"Carvão","Batata Palito Congelada","Danoninho","Yakult","Iogurte","Manteiga","Margarina",
"Pão de Queijo","Mortadela","Pizza","Presunto","Peito de Peru","Queijo Branco","Mussarela",
"Requeijão","Sorvete","Steak","Ovos","Ovos de Codorna","Abóbora","Batata Inglesa","Batata Doce",
"Berinjela","Beterraba","Brócolis","Carazinho","Cebola","Cenoura","Chuchu","Couve-Flor","Ervilha Fresca",
"Jiló","Mandioca / Aipim","Pepino","Pimentão","Quiabo","Reponho","Tomate","Vagem","Abacate",
"Abacaxi","Ameixa","Banana","Caju","Caqui","Goiaba","Laranja","Limão","Maçã","Mamão","Manga",
"Maracujá","Melancia","Melão","Morango","Pêra","Pêssego","Uva","Acelga","Agrião","Alface",
"Cheiro-Verde","Couve","Escarola","Espinafre","Rúcula","Achocolatado","Adoçante","Aveia",
"Biscoito","Café","Chocolate em Pó","Capuccino","Café Solúvel","Cereais","Neston","Farinha Lactea",
"Chá","Chocolate","Doces","Gelatina","Pudim","Flan","Geléia","Granola","Leite de Coco","Leite de Soja",
"Leite em Pó","Leite Integral","Leite Desnatado","Mel","Pão Francês","Pão de Fôrma","Sucrilhos","Torrada",
//Mercearia
"Açúcar Cristal","Açucar Refinado","Arroz","Batata Palha","Baunilha","Côco Rolado","Farinha de Mandioca",
"Farinha de Rosca","Farinha de Milho","Farinha de Trigo","Farofa Pronta","Feijão","Fermento",
"Fubá","Grão de Bico","Grão de Soja","Lentilha","Macarrão","Macarrão Instantâneo","Amido de Milho",
"Milho de Pipoca","Mistura p/ Bolo","Óleo","Pipoca Microondas","Queijo Ralado","Trigo p/ Quibe",
"Arroz Integral","Arroz Parborizado","Arroz de Risoto",
//Bebidas
"Água Mineral","Água de Côco","Aguardente","Cerveja","Refresco","Refrigerante","Suco","Vinho",
//Enlatados
"Atum","Sardinha","Azeitonas","Cogumelo","Creme de Leite","Ervilha","Leite Condensado",
"Maionese","Milho Verde","Molho de Tomate","Extrato de Tomate","Palmito","Patê",
"Polpa de Tomate"];

/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
autocomplete(document.getElementById("prod"), list);